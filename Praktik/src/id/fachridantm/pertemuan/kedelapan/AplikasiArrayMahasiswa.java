package id.fachridantm.pertemuan.kedelapan;

import java.util.Scanner;

public class AplikasiArrayMahasiswa {

    public static void main(String[] args) {
        int jumlahData, nim;
        String nama;
        double nilai;

        Scanner in = new Scanner(System.in);
        System.out.print("Masukan Jumlah Array = ");
        jumlahData = in.nextInt();

        Mahasiswa[] mahasiswas = new Mahasiswa[jumlahData];
        for (int i = 0; i <= jumlahData - 1; i++) {
            System.out.println("Mahasiswa ke : " + (i + 1));
            System.out.print("Masukan NIM = ");
            nim = in.nextInt();
            System.out.print("Masukan Nama = ");
            nama = in.next();
            System.out.print("Masukan Nilai = ");
            nilai = in.nextDouble();

            mahasiswas[i] = new Mahasiswa(nim, nama, nilai);
        }
        System.out.println("Data Mahasiswa pada Array");
        for (Mahasiswa mahasiswa : mahasiswas) {
            System.out.println("NIM : " + mahasiswa.getNim() + "\tNama : "
            + mahasiswa.getNama() + "\tNilai : " + mahasiswa.getNilai());
        }
    }
}
