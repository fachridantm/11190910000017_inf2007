package id.fachridantm.pertemuan.kedelapan;

import java.util.Scanner;

public class ArrayMaksimum {
    public static int getMaks(int A[], int n) {
        int i, maks;
        maks = -9999;
        for (i = 1; i <= n; i++) {
            if (A[i] > maks) {
                maks = A[i];
            } 
        }
        return maks;
    }
    
    public static void main(String[] args) {
        int A[] = new int[100];
        int i, n;
        Scanner in = new Scanner(System.in);
        
        
        System.out.print("Masukan banyaknya nilai: ");
        n = in.nextInt();
        
        for (i = 1; i <= n; i ++) {
            System.out.print("Masukan Angka: ");
            A[i] = in.nextInt();
        }
        System.out.println("Nilai maksimum: " + getMaks(A, n));
    }
}
