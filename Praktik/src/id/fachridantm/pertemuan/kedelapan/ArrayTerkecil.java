
package id.fachridantm.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Payoy
 */
public class ArrayTerkecil {
    

    int i, min = 9999, e;
    
    public int getTerkecil(int A[], int n){
        for (i = 0; i < A.length; i++) {
            if (A[i] < min) {
                min = A[i];
            }
        }
        e = min - 1;
        System.out.println();
        System.out.println("Elemen terkecil = " + min);
        System.out.println("Elemen yang lebih kecil dari elemen terkecil = " + e);
        
        return min;
    }
    
    public static void main(String[] args) {
        
        int i, n;
        
        Scanner in = new Scanner (System.in);
        ArrayTerkecil app = new ArrayTerkecil();
        
        System.out.print("N = ");
        n = in.nextInt();
        
        int[] A = new int[n];
        
        for (i = 0; i < A.length; i++) {
            System.out.print("Masukkan Array [" + i + "] : ");
            A[i] = in.nextInt();
        }
        
        System.out.println("\nMatriks Awal");
        for (i = 0; i < A.length; i++) {
            System.out.print(A[i] + "  ");
        }
        app.getTerkecil(A, n);
    }
}
