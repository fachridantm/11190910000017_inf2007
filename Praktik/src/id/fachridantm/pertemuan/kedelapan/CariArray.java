package id.fachridantm.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Payoy
 */
public class CariArray {
    
    int i, j = -1;

    public int getCari(int nilai[], int angka) {
        System.out.println("Nilai Pada Array:");
        for (i = 0; i < nilai.length; i++) {
            System.out.print(nilai[i] + "  ");
        }

        System.out.println("");
        for (i = 0; i < nilai.length; i++) {
            if (angka == nilai[i]) {
                j = i; 
                System.out.println("Angka yang dicari (" + angka + ") berada pada indeks ke: " + j);
             
            }
        }
        if (j == -1) {
                System.out.println("0");
        }
        return j;
    }

    public static void main(String[] args) {
        int x, n, i, j;
        Scanner in = new Scanner(System.in);
        CariArray app = new CariArray();

        System.out.print("N = ");
        n = in.nextInt();
        int[] nilai = new int[n];

        for (i = 0; i < nilai.length; i++) {
            System.out.print("Masukkan Array [" + i + "] : ");
            nilai[i] = in.nextInt();
        }

        System.out.print("Masukkan Angka yang ingin dicari : ");
        x = in.nextInt();

        app.getCari(nilai, x);
    }

}
