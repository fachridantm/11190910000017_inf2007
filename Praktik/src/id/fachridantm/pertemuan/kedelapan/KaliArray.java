package id.fachridantm.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Payoy
 */
public class KaliArray {
    
    int i, j;

    public void getInfo(int nilai[][]) {
        
        System.out.println("Matriks Awal");
        for (i = 0; i < nilai.length; i++) {
            for (j = 0; j < nilai[i].length; j++) {
                System.out.print(nilai[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public int[][]getKali(int nilai[][]) {
        for (i = 0; i < nilai.length; i++) {
            for (j = 0; j < nilai[i].length; j++) {
                nilai[i][j] = nilai[i][j] * 3;
            }
        }
        return nilai;
    }

    public void getHasil(int nilai[][]) {
        System.out.println("\nHasil Kali Matriks");
        for (i = 0; i < nilai.length; i++) {
            for (j = 0; j < nilai[i].length; j++) {
                System.out.print(nilai[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public static void main(String[] args) {
        int nilai[][] = new int[3][5];
        int i, j;
        KaliArray app = new KaliArray();
        Scanner in = new Scanner(System.in);

        for (i = 0; i < nilai.length; i++) {
            for (j = 0; j < nilai[i].length; j++) {
                System.out.print("Masukkan Array [" + i + "," + j + "] : ");
                nilai[i][j] = in.nextInt();
            }
        }

        app.getInfo(nilai);
        app.getKali(nilai);
        app.getHasil(nilai);
    }

}
