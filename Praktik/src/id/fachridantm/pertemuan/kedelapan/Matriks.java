package id.fachridantm.pertemuan.kedelapan;


public class Matriks {
    
    public static int[][] getPenambahanMatriks(int A[][], int B[][], int nBar, int nKol) {
        int i, j;
        int C[][] = new int[nBar][nKol];
        
        for (i = 0; i < nBar; i++) {
            for (j = 0; j < nKol; j++) {
                C[i][j] = B[i][j] + A[i][j];
            }
        }
        
        for (i = 0; i < nBar; i++) {
            System.out.println("[");
            for (j = 0; j < nKol; j++) {
                System.out.println(" " + C[i][j] + " ");
            }
            System.out.println("]");
        }
        return C;
    }
}
