package id.fachridantm.pertemuan.kedua;

import java.util.Scanner;

public class Mesin {
    
    public static void main(String args[]) {
        float PT, LT, PR, LR, W;
        
        Scanner in = new Scanner(System.in);
        System.out.println("Masukan panjang tanah: ");
        PT = in.nextFloat();
        System.out.println("Masukan lebar tanah: ");
        LT = in.nextFloat();
        System.out.println("Masukan panjang rumah: ");
        PR = in.nextInt();
        System.out.println("Masukan lebar rumah: ");
        LR = in.nextFloat();
        W = (float) (((PT * LT)-(PR * LR)) / 2.5);
        System.out.println("Waktu yang diperlukan untuk "
        + "memotong rumput adalah: " + W + " menit");
    }
}
