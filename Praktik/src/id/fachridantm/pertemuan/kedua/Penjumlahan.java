package id.fachridantm.pertemuan.kedua;

import java.util.Scanner;

public class Penjumlahan {

    public static void main(String args[]) {
        int a, b, c, d;

        Scanner in = new Scanner(System.in);
        System.out.println("Masukan 3 angka yang ingin dijumlahkan : ");
        a = in.nextInt();
        b = in.nextInt();
        c = in.nextInt();
        d = a + b + c;
        System.out.println("Hasilnya adalah: " + d );
    }
}