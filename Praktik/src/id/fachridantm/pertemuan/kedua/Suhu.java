package id.fachridantm.pertemuan.kedua;

import java.util.Scanner;

public class Suhu {

    public static void main(String args[]) {
        int a, b, c;

        Scanner in = new Scanner(System.in);
        System.out.println("Masukan suhu minimum pada hari H: ");
        a = in.nextInt();
        System.out.println("Masukan suhu maksimum pada hari H: ");
        b = in.nextInt();
        c = (a + b) / 2;
        System.out.println("Rata-rata suhu pada hari H adalah: " + c);
    }
}
