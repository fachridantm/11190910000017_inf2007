package id.fachridantm.pertemuan.keduabelas;

/**
 *
 * @author Payoy
 */
public class Bayaran {
     public int hitungBayaran(Pegawai pegawai) {
         int uang = pegawai.infoGaji();
         if (pegawai instanceof Manajer) {
             uang += ((Manajer) pegawai).infoTunjangan();
         } else if (pegawai instanceof Programmer) {
             uang += ((Programmer) pegawai).infoBonus();
         }
         return uang;
     }
     
     public static void main(String[] args) {
        Manajer m = new Manajer("Budi", 800, 50);
        Programmer p = new Programmer("Cecep", 600, 30);
        Bayaran upah = new Bayaran();
        System.out.println("Upah Manajer : " + upah.hitungBayaran(m));
        System.out.println("Upah Programmer : " + upah.hitungBayaran(p));
    }
}
