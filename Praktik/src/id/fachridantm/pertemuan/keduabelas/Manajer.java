package id.fachridantm.pertemuan.keduabelas;

/**
 *
 * @author Payoy
 */
public class Manajer extends Pegawai {
    private final int tunjangan;

    public Manajer(String nama, int gaji, int tunjangan) {
        super(nama, gaji);
        this.tunjangan = tunjangan;
    }
    public int infoGaji() {
        return gaji;
    }
    
    public int infoTunjangan() {
        return tunjangan;
    }
}
