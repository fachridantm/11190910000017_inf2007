package id.fachridantm.pertemuan.keduabelas;

/**
 *
 * @author Payoy
 */
public class PenyimpananTest {
    public static void main(String[] args) {
        PenyimpananUang tabungan = new PenyimpananUang(5000, 8.5 / 100);
        System.out.println("Uang yang ditabung : 5000");
        System.out.println("Tingkat bunga sekarang : 8.50");
        System.out.println("Total uang Anda sekarang : " + tabungan.cekUang());
    }
}
