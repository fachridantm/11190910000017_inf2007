package id.fachridantm.pertemuan.keduabelas;

/**
 *
 * @author Payoy
 */
public class PenyimpananUang extends Tabungan {
    private final double tingkatBunga;

    public PenyimpananUang(int saldo, double tingkatBunga) {
        super(saldo);
        this.tingkatBunga = tingkatBunga;
    }
    
    public double cekUang() {
        return this.saldo + this.saldo * this.tingkatBunga;
    }
}   
