package id.fachridantm.pertemuan.keduabelas;

/**
 *
 * @author Payoy
 */
public class Tabungan {
    protected int saldo;
    
    public Tabungan (int saldo) {
        this.saldo = saldo;
    }
    
    public int ambilUang (int jumlah) {
        this.saldo = saldo - jumlah;
        return this.saldo;
    }
}
