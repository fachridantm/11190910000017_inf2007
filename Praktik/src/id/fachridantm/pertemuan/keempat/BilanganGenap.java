package id.fachridantm.pertemuan.keempat;

import java.util.Scanner;

public class BilanganGenap {
    public static void main(String[] args) {
        int bilangan;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan bilangan untuk dicek: ");
        bilangan = in.nextInt();
        if (bilangan % 2 == 0) {
        System.out.println("Angka " + bilangan + 
                    " adalah Bilangan Genap");
        }
    }
}
