package id.fachridantm.pertemuan.keempat;

import java.util.Scanner;

public class BilanganKelipatanTiga {
    public static void main(String[] args) {
        int a;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan bilangan untuk dicek: ");
        a = in.nextInt();
        if (a % 3 == 0) {
            System.out.println("Angka " + a + 
                    " adalah Kelipatan Bilangan Tiga");            
        }
    }
}
