package id.fachridantm.pertemuan.keempat;

import java.util.Scanner;

public class BilanganTerbesar {

    public static void main(String[] args) {
        int a, b, c, maks;
        Scanner in = new Scanner(System.in);

        System.out.println("Masukan angka yang ingin diurutkan: ");
        a = in.nextInt();
        b = in.nextInt();
        c = in.nextInt();

        if ((a > b) && (a > c)) {
            System.out.println("Bilangan terbesar: " + a);
        } else {
            if ((b > a) && (b > c)) {
                System.out.println("Bilangan terbesar: " + b);
            } else {
                System.out.println("Bilangan terbesar: " + c);
            }
        }
    }
}