package id.fachridantm.pertemuan.keempat;

import java.util.Scanner;

public class DiskonBelanja {
    public static void main(String[] args) {
        int totalbelanja, diskonharga, nilaibelanja;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan total belanja Anda: ");
        totalbelanja = in.nextInt();
        if (totalbelanja > 120000) {
            diskonharga = totalbelanja * 7/100;
            nilaibelanja = totalbelanja - diskonharga;
            System.out.println("Selamat Anda mendapatkan diskon sebesar Rp "
                    + diskonharga);
            System.out.println("Jadi, total belanja Anda adalah: Rp "
                    + nilaibelanja);
        } else {
            System.out.println("Maaf, Anda tidak mendapatkan diskon");
            System.out.println("Total belanja Anda adalah: "  + totalbelanja);
        }
    }
}
