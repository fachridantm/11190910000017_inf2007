package id.fachridantm.pertemuan.keempat;

import java.util.Scanner;

public class GolonganUpahKaryawan {
    public static void main(String[] args) {
        final int jamNormal, upahLembur;
        jamNormal = 48;
        upahLembur = 3000;
        String nama;
        char gol = 0;
        int jjk, jamLembur;
        float upahPerJam, upahTotal;
        upahPerJam = 0;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan Nama Karyawan: ");
        nama = in.nextLine();
        System.out.println("Masukan Golongan: ");
        gol = in.next().charAt(gol);
        System.out.println("Masukan Jumlah Jam Kerja: ");
        jjk = in.nextInt();
        if (gol == 'A') {
            upahPerJam = 4000;
        } else {
            if (gol == 'B') {
                upahPerJam = 5000;
            } else {
                if (gol == 'C') {
                   upahPerJam = 6000;
                    } else {
                        if (gol == 'D') {
                            upahPerJam = 7500;
                    }
                }    
            } 
        }
        if (jjk <= jamNormal) {
            upahTotal = jjk * upahPerJam;
        } else {
            jamLembur = jjk - jamNormal;
            upahTotal = (jamNormal * upahPerJam) + (jamLembur * upahLembur);
        }
        System.out.println("Upah total dari saudara"
                + " " + nama + " adalah " + upahTotal);
    }
}
