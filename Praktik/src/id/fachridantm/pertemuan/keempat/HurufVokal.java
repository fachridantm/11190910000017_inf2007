package id.fachridantm.pertemuan.keempat;

import java.util.Scanner;

public class HurufVokal {
    public static void main(String[] args) {
        char k = 0;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan huruf yang ingin dicek: ");
        k = in.next().charAt(k);
        if ((k == 'a' ) || (k == 'i') || (k == 'u') || (k == 'e') || (k == 'o')) {
            System.out.println("Huruf " + k + " adalah huruf vokal");
        }
    }
}
