package id.fachridantm.pertemuan.keempat;

import java.util.Scanner;

public class IndeksNilaiUjian {
    public static void main(String[] args) {
        float nilai;
        char indeks;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan nilai ujian Anda: ");
        nilai = in.nextFloat();
        if (nilai >= 80) {
            indeks = 'A';
        } else {
            if (nilai >= 70) {
                indeks = 'B';
            } else {
                if (nilai >= 55) {
                    indeks = 'C';
                } else {
                    if (nilai >= 40) {
                        indeks = 'D';
                    } else {
                        indeks = 'E';
                    }
                }
            }
        }
        System.out.println("Indeks nilai ujian Anda adalah " + indeks);
    }
}
