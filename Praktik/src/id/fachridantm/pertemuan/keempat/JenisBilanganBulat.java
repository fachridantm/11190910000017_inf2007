package id.fachridantm.pertemuan.keempat;

import java.util.Scanner;

public class JenisBilanganBulat {
    public static void main(String[] args) {
        int bilangan;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan angka yang ingin dicek: ");
        bilangan = in.nextInt();
        if (bilangan > 0) {
            System.out.println(bilangan + " adalah bilangan positif");
        } else {
            if (bilangan < 0) {
                System.out.println(bilangan + " adalah bilangan negatif");
            } else {
                if (bilangan == 0) {
                    System.out.println(bilangan + " adalah nol");
                }
            }
        }
    }
}
