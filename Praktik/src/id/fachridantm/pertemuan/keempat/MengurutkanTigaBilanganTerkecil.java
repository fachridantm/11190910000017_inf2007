package id.fachridantm.pertemuan.keempat;

import java.util.Scanner;

public class MengurutkanTigaBilanganTerkecil {
    public static void main(String[] args) {
        int bilangan1, bilangan2, bilangan3;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan tiga bilangan yang ingin diurutkan!");
        System.out.println("Bilangan pertama: ");
        bilangan1 = in.nextInt();
        System.out.println("Bilangan kedua: ");
        bilangan2 = in.nextInt();
        System.out.println("Bilangan ketiga: ");
        bilangan3 = in.nextInt();
        if (bilangan1 <= bilangan2 && bilangan2 <= bilangan3) {
            System.out.println(bilangan1 + ", " + bilangan2 + ", " + bilangan3);
        } else {
            if (bilangan1 <= bilangan3 && bilangan3 <= bilangan2) {
                System.out.println(bilangan1 + ", " + bilangan3 + ", " + bilangan2);
            } else {
                if (bilangan2 <= bilangan1 && bilangan1 <= bilangan3) {
                    System.out.println(bilangan2 + ", " + bilangan1 + ", " + bilangan3);
                } else {
                    if (bilangan2 <= bilangan3 && bilangan3 <= bilangan1) {
                        System.out.println(bilangan2 + ", " + bilangan3 + ", " + bilangan1);
                    } else {
                        if (bilangan3 <= bilangan1 && bilangan1 <= bilangan2) {
                            System.out.println(bilangan3 + ", " + bilangan1 + ", " + bilangan2);
                        } else {
                            if (bilangan3 <= bilangan2 && bilangan2 <= bilangan1) {
                                System.out.println(bilangan3 + ", " + bilangan2 + ", " + bilangan1);
                            }
                        }
                    }
                }
            }
        }
    }
}