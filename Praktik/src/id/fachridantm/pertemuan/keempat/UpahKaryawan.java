package id.fachridantm.pertemuan.keempat;

import java.util.Scanner;

public class UpahKaryawan {
    public static void main(String[] args) {
        String nama;
        int jjk;
        float lembur, upah;
        final int jamNormal, upahPerJam, upahLembur;
        jamNormal = 48;
        upahPerJam = 2000;
        upahLembur = 3000;
                
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan Nama Karyawan: ");
        nama = in.nextLine();
        System.out.println("Masukan Jumlah Jam Kerja: ");
        jjk = in.nextInt();
        if (jjk <= jamNormal) {
            upah = jjk * upahPerJam;
        } else {
            lembur = jjk - jamNormal;
            upah = jamNormal * upahPerJam + lembur + upahLembur;
        }
        System.out.println("Upah kerja " + nama + " selama " + jjk 
                + " jam adalah Rp " + upah);
    }
}