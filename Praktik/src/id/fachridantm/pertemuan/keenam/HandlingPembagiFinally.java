package id.fachridantm.pertemuan.keenam;

public class HandlingPembagiFinally {
    public static void main(String[] args) {
        try {
            int a = 10;
            int b = 0;
            int c = a / b;

            System.out.println("Hasil: " + c);
        } catch (Throwable error) {
            System.out.print("Ups, terjadi error: ");
            System.out.println(error.getMessage());
        } finally {
            System.out.println("Pasti akan dijalankan");
        }
    }
}
