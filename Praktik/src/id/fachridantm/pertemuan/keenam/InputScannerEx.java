package id.fachridantm.pertemuan.keenam;

import java.util.Scanner;

public class InputScannerEx {
    public static void main(String[] args) {
        int bilangan;
        Scanner in = new Scanner(System.in);
        System.out.print("Masukan bilangan: ");
        bilangan = in.nextInt();
        
        System.out.println("Bilangan: " + bilangan);
    }
}
