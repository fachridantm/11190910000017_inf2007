package id.fachridantm.pertemuan.keenam;

import javax.swing.JOptionPane;

public class OutputJOptionPaneEx {
    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("Masukan bilangan: ");
        
        bilangan = Integer.parseInt(box);
        
        JOptionPane.showMessageDialog(null, "Bilangan: " + bilangan,
                "Hasil Input", JOptionPane.INFORMATION_MESSAGE);
    }
}