package id.fachridantm.pertemuan.kelima;

import java.util.Scanner;

public class CariMinimum {
    public static void main(String[] args) {
        int N, x, min, i;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan banyaknya data (N) yang ingin diinput, "
                + "(N > 0): ");
        N = in.nextInt();
        System.out.println("Input data: ");
        x = in.nextInt();
        min = x;
        for (i = 2; i <= N; i ++) {
            System.out.println("Input data selanjutnya: ");
            x = in.nextInt();
            if (x < min) {
                min = x;
            }
        }
        System.out.println("Dari data inputan nilai minimum adalah: " + min);
    }
}
