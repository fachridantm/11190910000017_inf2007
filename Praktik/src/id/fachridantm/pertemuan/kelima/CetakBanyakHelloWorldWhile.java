package id.fachridantm.pertemuan.kelima;

public class CetakBanyakHelloWorldWhile {
    public static void main(String[] args) {
        int i;
        i = 1;
        
        while (i <= 10) {
            System.out.println("Hello, world");
            i = i + 1;
        }
    }
}
