package id.fachridantm.pertemuan.kelima;

import java.util.Scanner;

public class HitungRerata {
    public static void main(String[] args) {
        int i, x, jumlah;
        i = 0;
        jumlah = 0;
        float rerata;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan nilai Anda: ");
        x = in.nextInt();
        while (x != -1) {
            i = i + 1; 
            jumlah = jumlah + x;  
            x = in.nextInt();
        }
        if (i != 0) {
            rerata = jumlah/i;
            System.out.println("Rata-rata nilai Anda adalah: " + rerata);
        } else {
            System.out.println("Tidak ada nilai ujian yang dimasukkan!");
        }
    }
}
