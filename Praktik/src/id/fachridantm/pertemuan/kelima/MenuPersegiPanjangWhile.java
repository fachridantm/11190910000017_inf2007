package id.fachridantm.pertemuan.kelima;

import static java.lang.Math.sqrt;
import java.util.Scanner;

public class MenuPersegiPanjangWhile {
    public static void main(String[] args) {
        int noMenu = 0;
        float panjang, lebar, luas, keliling, diagonal;
        Scanner in = new Scanner(System.in);

        while (noMenu != 4) {
            System.out.println("===== Menu Persegi Psnjang =====");
            System.out.println("1. Hitung Luas");
            System.out.println("2. Hitung Keliling");
            System.out.println("3. Hitung Panjang Diagonal");
            System.out.println("4. Keluar Program");
            System.out.println("Masukan menu pilihan anda: ");
            noMenu = in.nextInt();
            switch (noMenu) {
                case 1:
                    System.out.println("Masukan panjang: ");
                    panjang = in.nextFloat();
                    System.out.println("Masukan lebar: ");
                    lebar = in.nextFloat();
                    luas = panjang * lebar;
                    System.out.println("Luas dari "
                            + "persegi panjang tersebut adalah: " + luas);
                    break;
                case 2:
                    System.out.println("Masukan panjang: ");
                    panjang = in.nextFloat();
                    System.out.println("Masukan lebar: ");
                    lebar = in.nextFloat();
                    keliling = ((2 * panjang) + (2 * lebar));
                    System.out.println("Keliling dari "
                            + "persegi panjang tersebut adalah: " + keliling);
                    break;
                case 3:
                    System.out.println("Masukan panjang: ");
                    panjang = in.nextFloat();
                    System.out.println("Masukan lebar: ");
                    lebar = in.nextFloat();
                    diagonal = (float) sqrt(panjang * (panjang + lebar) * lebar);
                    System.out.println("Panjang diagonal dari "
                            + "persegi panjang tersebut adalah: " + diagonal);
                    break;
                case 4:
                    System.out.println("Sampai Jumpa!");
                    break;
            }
        }
    }
}
