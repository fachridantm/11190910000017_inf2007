package id.fachridantm.pertemuan.kelima;

public class PeluncurRoketWhile {
    public static void main(String[] args) {
        int i;
        
        i = 10;
        while (i >= 0) {
            System.out.println(i);
            i = i - 1;
        }
        System.out.println("Go");
    }
}
