package id.fachridantm.pertemuan.kelima;

import java.util.Scanner;

public class PenjumlahanBilanganGanjil {
    public static void main(String[] args) {
        int N, i, g, jumlah = 0;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan banyaknya nilai (N) bilangan ganjil"
                + " yang ingin dijumlahkan : (N > 0)");
        N = in.nextInt();
        g = N * 2;
        System.out.print(N + " bilangan ganjil pertamanya adalah: ");
        for (i = 1; i <= g; i++) {
            if (i % 2 != 0) {
                System.out.print(i + ", ");
                jumlah = jumlah + i;
            }
        }
        System.out.println("\nJumlah dari " + N + " bilangan ganjil pertama"
                + " adalah: " + jumlah);
    }
}
