package id.fachridantm.pertemuan.kelima;

import java.util.Scanner;

public class PenjumlahanDeretPecahanRepeat {
    public static void main(String[] args) {
        int x;
        float s;
        s = 0;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan angka: ");
        do {
            x = in.nextInt();
            s = s + (float) 1/x;
        } while (x != -1);
        System.out.println(s);
    }
}
