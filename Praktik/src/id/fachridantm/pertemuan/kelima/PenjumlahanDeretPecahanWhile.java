package id.fachridantm.pertemuan.kelima;

import java.util.Scanner;

public class PenjumlahanDeretPecahanWhile {
    public static void main(String[] args) {
        int x;
        float s;
        s = 0;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan angka: ");
        x = in.nextInt();
        while (x != -1) {
            s = s + (float) 1/x;
            x = in.nextInt();
        }
        System.out.println(s);
    }
}
