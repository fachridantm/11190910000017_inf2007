package id.fachridantm.pertemuan.kelima;

import java.util.Scanner;

public class Perpangkatan {
    public static void main(String[] args) {
        int n, i;
        float a, p;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan nilai yang ingin dipangkatkan: ");
        a = in.nextFloat();
        System.out.println("Masukan angka pemangkat: ");
        n = in.nextInt();
        p = 1;
        for (i = 1; i <= n; i++) {
            p = p * a;
        }
        System.out.println(p);
    }
}
