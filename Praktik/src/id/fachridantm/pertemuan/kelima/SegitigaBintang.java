package id.fachridantm.pertemuan.kelima;

import java.util.Scanner;

public class SegitigaBintang {
    public static void main(String[] args) {
        int n, i, a;
        Scanner in = new Scanner(System.in);
        
        System.out.print("Masukan jumlah banyaknya baris: ");
        n = in.nextInt();
        for (i = 1; i <= n; i++) {
            for (a = 1; a <= i; a++) {
                System.out.print(" " + a);
            }
            System.out.println();
        }
    }
}
