package id.fachridantm.pertemuan.kelima;

import java.util.Scanner;

public class ValidasiSandi {

    public static void main(String[] args) {
        int count = 1;
        boolean sah = false;
        String sandi;
        String password = "abc123";
        Scanner in = new Scanner(System.in);

        while ((!sah) && (count <= 3)) {
            System.out.println("Masukan kata sandi Anda: ");
            sandi = in.nextLine();
            if (sandi.equals(password)) {
                sah = true;
                System.out.println("Kata sandi Anda benar");
            } else {
                count = count + 1;
                System.out.println("Kata sandi Anda salah! Coba lagi!");
            }
        }
    }
}
