package id.fachridantm.pertemuan.kesebelas;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class ArsipTeks {
    int getHitungKarakter(FileReader T) {
        int n;
        char C[];
        
        n = 0;
        Scanner line = new Scanner(new BufferedReader(T));
        while (line.hasNext()) {
            C = line.next().toCharArray();
            for (char d : C) {
                if (d == 'a') {
                    n = n + 1;
                }
            }
        }
        return n;
    }
    
    public static void main(String[] args) {
        ArsipTeks baca = new ArsipTeks();
        int a;
        
        try {
            PrintWriter outFile = new PrintWriter(new FileOutputStream("D:\\Documents\\Payoy\\Tugas Kuliah\\Dasar - Dasar Pemrograman\\temp\\T.txt"));
            outFile.println("aaaaa");
            outFile.close();
            System.out.print("Banyak nya 'a' = ");
            a = baca.getHitungKarakter(new FileReader("D:\\Documents\\Payoy\\Tugas Kuliah\\Dasar - Dasar Pemrograman\\temp\\T.txt"));
            System.out.println(a);
            
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
    }
}
