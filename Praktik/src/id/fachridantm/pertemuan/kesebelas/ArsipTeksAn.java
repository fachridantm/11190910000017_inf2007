package id.fachridantm.pertemuan.kesebelas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author Payoy
 */
public class ArsipTeksAn {

    boolean getCariKarakter(String File) throws FileNotFoundException {
        boolean ketemu = false;
        char[] L;
        Scanner line = new Scanner(new BufferedReader(new FileReader(File)));
        int n = 0;
        while (line.hasNext()) {
            L = line.next().toCharArray();
            for (int i = 0; i < L.length; i++) {
                if (L[i] == 'a') {
                    if (L[i + 1] == 'n') {
                        ketemu = true;
                    }
                }
            }

        }
        return ketemu;
    }

    public static void main(String[] args) throws FileNotFoundException {
        String Kata;

        String File = "D:\\Documents\\Payoy\\Tugas Kuliah\\Dasar - Dasar Pemrograman\\temp\\An.txt";
        Scanner in = new Scanner(System.in);
        ArsipTeksAn baca = new ArsipTeksAn();
        
        try {
            PrintWriter outFile = new PrintWriter(new FileOutputStream(File));

            System.out.print("Masukkan Kata: ");
            Kata = in.nextLine();

            outFile.println(Kata);
            outFile.close();

        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
        System.out.println("\nHuruf An terdapat dalam Kata tersebut? " + baca.getCariKarakter(File));
    }
}
