package id.fachridantm.pertemuan.kesebelas;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class BacaArsipBilangan {
    public static void main(String[] args) {
        int x;
        
        try {
            BufferedReader file = new BufferedReader(new FileReader("D:\\Documents\\Payoy\\Tugas Kuliah\\Dasar - Dasar Pemrograman\\temp\\Tulis.txt"));
            Scanner line = new Scanner(file);
            while ((line.hasNextInt())) {
                x = line.nextInt();
                System.out.println(x);
            }
            file.close();
            
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
    }
}
