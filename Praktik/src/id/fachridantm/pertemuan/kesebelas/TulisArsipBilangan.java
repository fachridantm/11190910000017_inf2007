package id.fachridantm.pertemuan.kesebelas;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class TulisArsipBilangan {    
    public static void main(String[] args) {
        int i, n;
        Scanner in = new Scanner(System.in);
        
        try {
            PrintWriter outFile = new PrintWriter(new FileOutputStream("D:\\Documents\\Payoy\\Tugas Kuliah\\Dasar - Dasar Pemrograman\\temp\\Tulis.txt"));
            System.out.print("Masukan nilai n: ");
            n = in.nextInt();
            
            for (i = 1; i <= n; i++) {
                outFile.println(i);
            }
            outFile.close();
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
            
        }
    }
}
