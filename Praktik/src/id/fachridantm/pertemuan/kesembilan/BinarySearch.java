package id.fachridantm.pertemuan.kesembilan;

import java.util.Scanner;

public class BinarySearch {

    public int getBinarySearch(int L[], int n, int x) {
        int i = 0, j = n, k = 0;
        boolean ketemu = false;

        while ((!ketemu) && (i <= j)) {
            k = (i + j) / 2;
            if (L[k] == x) {
                ketemu = true;
            } else {
                if (L[k] > x) {
                    i = k++;
                } else {
                    j = --k;
                }
            }
        }
        if (ketemu) {
            return k;
        } else {
            System.out.println("NILAI SALAH!");
            return -1;
        }
    }

    public static void main(String[] args) {
        int L[] = {81, 76, 21, 18, 16, 13, 10, 7};
        int n = L.length;

        BinarySearch search = new BinarySearch();
        Scanner in = new Scanner(System.in);
        System.out.print("Masukan angka yang ingin dicari: ");
        int x = in.nextInt();

        System.out.println("Nilai " + x + " ada di indeks "
                + search.getBinarySearch(L, n, x));
    }
}
