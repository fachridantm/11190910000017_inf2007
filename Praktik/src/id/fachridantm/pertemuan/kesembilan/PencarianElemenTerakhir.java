package id.fachridantm.pertemuan.kesembilan;

import java.util.Scanner;

public class PencarianElemenTerakhir {

    public int getElemenTerakhir(int L[], int n, int x) {
        int i = n - 1;
        boolean ketemu = false;

        while ((i > -1) && (i <= n - 1) && (!ketemu)) {
            if (i <= n - 1) {
                System.out.println("Posisi ke-" + i + " isinya " + L[i]);
            }
            if (L[i] == x) {
                ketemu = true;
            } else {
                --i;
            }
        }
        if (ketemu) {
            return i;
        } else {
            return -1;
        }
    }

    public static void main(String[] args) {
        int L[] = {13, 16, 14, 21, 76, 15};
        int n = L.length;

        Scanner in = new Scanner(System.in);
        System.out.print("Masukan angka yang ingin dicari: ");
        int x = in.nextInt();

        PencarianElemenTerakhir search = new PencarianElemenTerakhir();
        System.out.println("Nilai: " + search.getElemenTerakhir(L, n, x));
    }
}
