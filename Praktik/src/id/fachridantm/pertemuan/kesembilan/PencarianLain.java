package id.fachridantm.pertemuan.kesembilan;

import java.util.Scanner;

public class PencarianLain {

    public int getPencarianLain(int L[], int x) {
        int i, idx = -1;

        for (i = 0; i < L.length; i++) {
            if (L[i] == x) {
                idx = i;
            }
        }
        return idx;
    }

    public static void main(String[] args) {
        int L[] = {13, 16, 14, 21, 76, 15};
        int x, n = L.length;
        
        Scanner in = new Scanner(System.in);
        PencarianLain app = new PencarianLain();
        
        System.out.print("Masukan x = ");
        x = in.nextInt();
        
        System.out.println("idx = " + app.getPencarianLain(L, x));
    }
}
