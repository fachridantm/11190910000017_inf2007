package id.fachridantm.pertemuan.kesembilan;

import java.util.Scanner;

public class PencarianString {
    public int SequentialSeacrh (String L[], String x, int n) {
        int i = 0;
        
        while ((i <= n - 1) && (!L[i].equals(x))) {
            i++;
        }
        if (L[i].equals(x)) {
            return i;
        } else {
            return -1;
        }
    }
    
    public int BinarySearch(String L[], String x, int n) {
        int i = 0;
        int j = n;
        int k = 0;
        boolean found = false;
        
          while ((!found) && (i <= j)) {
            k = (i + j) / 2;
            if (L[k].equals(x)) {
                found = true;
            } else {
                if (L[k].compareTo(x) < 0) {
                    i = k++;
                } else {
                    j = --k;
                }
            }
        }
        if (found) {
            return k;
        } else {
            return -1;
        }
    }
    
    public static void main(String[] args) {
        String L[] = {"A", "B", "C", "D", "E", "F"};
        int n = L.length;
        
        String x;
        System.out.print("Masukan Huruf yg ingin dicari: ");
        Scanner in = new Scanner(System.in);
        x = in.nextLine();
        
        PencarianString cari = new PencarianString();
        System.out.println("Hasil pencarian beruntunnya:"); 
        System.out.println("Ada di Indeks ke- " + cari.SequentialSeacrh(L, x, n));
        System.out.println("Hasil pencarian bagi dua:"); 
        System.out.println("Ada di Indeks ke- " + cari.BinarySearch(L, x, n));
    }
}
