package id.fachridantm.pertemuan.kesembilan;

import java.util.Scanner;

public class SequentialSearch {

    public boolean getSeqSearchBoolean(int L[], int n, int x) {
        int i = 0;

        while ((i < n - 1) && (L[i] != x)) {
            if (i == 0) {
                System.out.println("Posisi ke-" + i + " isinya " + L[i]);
            }
            i++;
            System.out.println("Posisi ke-" + i + " isinya " + L[i]);
        }
        if (L[i] == x) {
            return true;
        } else {
            return false;
        }
    }

    public int getSeqSearch(int L[], int n, int x) {
        int i = 0;

        while ((i < n - 1) && (L[i] != x)) {
            if (i == 0) {
                System.out.println("Posisi ke-" + i + " isinya " + L[i]);
            }
            i++;
            System.out.println("Posisi ke-" + i + " isinya " + L[i]);
        }
        if (L[i] == x) {
            return i;
        } else {
            return -1;
        }
    }

    public boolean getSeqSearchBoolean2(int L[], int n, int x) {
        int i = 0;
        boolean ketemu = false;

        while ((i < n - 1) && (!ketemu)) {
            if (i == 0) {
                System.out.println("Posisi ke-" + i + " isinya " + L[i]);
            }
            if (L[i] == x) {
                ketemu = true;
            } else {
                i++;
                System.out.println("Posisi ke-" + i + " isinya " + L[i]);
            }
        }
        return ketemu;
    }

    public int getSeqSearch2(int L[], int n, int x) {
        int i = 0;
        boolean ketemu = false;

        while ((i <= n - 1) && (!ketemu)) {
            if (i == 0) {
                System.out.println("Posisi ke-" + i + " isinya " + L[i]);
            }
            if (L[i] == x) {
                ketemu = true;
            } else {
                i++;
                System.out.println("Posisi ke-" + i + " isinya " + L[i]);
            }
        }
        if (ketemu) {
            return i;
        } else {
            return -1;
        }
    }

    public int getSeqSearchSentinel(int L[], int n, int x) {
        int idx, i = 0;
        L[n] = x;   
        
        while (L[i] != x) {
            i++;
            System.out.println("Posisi ke-" + i + " isinya " + L[i]);
        }
        if (i == n) {
            idx = -1;
        } else {
            idx = i;
        }
        return idx;
    }

    public static void main(String[] args) {
        //int L[] = {13, 14, 16, 21, 76, 15};
        int L[] = new int[7];
        L[0] = 13;
        L[1] = 14;
        L[2] = 16;
        L[3] = 21;
        L[4] = 76;
        L[5] = 15;
        int n = 6;

        Scanner in = new Scanner(System.in);
        System.out.print("Masukan angka yang ingin dicari: ");
        int x = in.nextInt();

        SequentialSearch search = new SequentialSearch();
        //System.out.println("Nilai: " + search.getSeqSearchBoolean(L, n, x));
        //System.out.println("Nilai: " + search.getSeqSearch(L, n, x));
        //System.out.println("Nilai: " + search.getSeqSearchBoolean2(L, n, x));
        //System.out.println("Nilai: " + search.getSeqSearch2(L, n, x));
        System.out.println("Nilai: " + search.getSeqSearchSentinel(L, n, x));
    }
}
