package id.fachridantm.pertemuan.kesepuluh;

import java.util.Arrays;


public class BubbleSort {
    public int[] getBubbleSort(int L[], int n) {
        int i, k, temp;
        
        for (i = 0; i < n - 1; i++) {
            for (k = n - 1; k > i; k--) {
                System.out.println("i: " + i + ", k: " + (k-1) + " --> " + L[k-1]);
                if (L[k] < L[k-1]) {
                    temp = L[k];
                    L[k] = L[k-1];
                    L[k-1] = temp;
                }
            }
        }
        return L;
    }
    public static void main(String[] args) {
        int L[] = {25, 27, 10, 8, 76, 21};
        int n = L.length;
        
        BubbleSort sort = new BubbleSort();
        System.out.println("Angka sebelum terurut: " + Arrays.toString(L));
        sort.getBubbleSort(L, n);
        System.out.println("Angka terurut: " + Arrays.toString(L));
    }
}