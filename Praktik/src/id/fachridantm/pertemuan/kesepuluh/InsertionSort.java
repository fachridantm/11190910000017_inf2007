package id.fachridantm.pertemuan.kesepuluh;

import java.util.Arrays;

public class InsertionSort {
    
    public int[] getInsertionSort(int L[], int n) {
        int i, j, y;
        boolean found;
        
        for (i = 1; i < n; i++) {
            y = L[i];
            j = i - 1;
            found = false;
            
            while ((j >= 0) && (!found)) {
                System.out.println("i: " + i + ", j: " + j + " --> " + L[j]);
                if (y < L[j]) {
                    L[j + 1] = L[j];
                    j = j - 1;
                } else {
                    found = true;
                }
            }
            L[j + 1] = y;
        }
        return L;
    }
    
    public static void main(String[] args) {
        int L[] = {25, 27, 10, 8, 76, 21};
        int n = L.length;
        
        InsertionSort sort = new InsertionSort();
        System.out.println("Angka acak: " + Arrays.toString(L));
        sort.getInsertionSort(L, n);
        System.out.println("Angka terurut: " + Arrays.toString(L));
    }
}
