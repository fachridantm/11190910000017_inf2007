package id.fachridantm.pertemuan.kesepuluh;

import java.util.Arrays;

public class SelectionSort {

    public int[] getSelectionSortMax(int L[], int n) {
        int i, j, imaks, temp;

        for (i = n - 1; i > 1; i--) {
            imaks = 0;
            for (j = 1; j < i; j++) {
                System.out.println("i: " + i + ", j: " + j + " --> " + L[j]);
                if (L[j] > L[imaks]) {
                    imaks = j;
                }
            }
            temp = L[i];
            L[i] = L[imaks];
            L[imaks] = temp;
        }
        return L;
    }

    public int[] getSelectionSortMin(int L[], int n) {
        int i, j, imin, temp;

        for (i = 0; i < n - 1; i++) {
            imin = i;
            for (j = i; j < n; j++) {
                System.out.println("i: " + i + ", j: " + j + " --> " + L[j]);
                if (L[j] < L[imin]) {
                    imin = j;
                }
            }
            temp = L[i];
            L[i] = L[imin];
            L[imin] = temp;
        }
        return L;
    }

    public static void main(String[] args) {
        int L[] = {25, 27, 10, 8, 76, 21};
        int n = L.length;

        SelectionSort sort = new SelectionSort();
        //System.out.println("Angka sebelum terurut: " + Arrays.toString(L));
        //sort.getSelectionSortMax(L, n);
        //System.out.println("Angka terurut: " + Arrays.toString(L));
        System.out.println("Angka sebelum terurut: " + Arrays.toString(L));
        sort.getSelectionSortMin(L, n);
        System.out.println("Angka terurut: " + Arrays.toString(L));
    }
}
