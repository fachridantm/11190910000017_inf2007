package id.fachridantm.pertemuan.kesepuluh;

import java.util.Arrays;

public class ShellSort {

    public int[] getShellSort(int L[], int n) {
        int step = n - 1, start;
        int i, j, y;
        boolean found;

        while (step > 1) {
            step = step / 3 + 1;
            for (start = 0; start <= step; start++) {
                i = start + step;

                while (i <= n - 1) {
                    y = L[i];
                    j = i - step;
                    found = false;

                    while ((j >= 0) && (!found)) {
                        System.out.println("i: " + i + ", j: " + j + " --> " + L[j]);
                        if (y < L[j]) {
                            L[j + step] = L[j];
                            j = j - step;
                        } else {
                            found = true;
                        }
                    }
                    L[j + step] = y;
                    i = i + step;
                }
            }
        }
        return L;
    }

    public static void main(String[] args) {
        int L[] = {25, 27, 10, 8, 76, 21};
        int n = L.length;

        ShellSort sort = new ShellSort();
        System.out.println("Angka acak: " + Arrays.toString(L));
        sort.getShellSort(L, n);
        System.out.println("Angka terurut: " + Arrays.toString(L));
    }
}
