package id.fachridantm.pertemuan.ketiga;

import java.util.Scanner;

public class KonversiJarak {
    public static void main(String[] args) {
        int jarak, km, m, cm, sisa_jarak;
        final int m1, km1;
        m1 = 100;
        km1 = 100000;
        
        Scanner in = new Scanner(System.in);
        System.out.println("Masukan jarak yg ditempuh (dalam cm): ");
        jarak = in.nextInt();
        km = jarak / km1;
        sisa_jarak = jarak % km1;
        System.out.println("Jarak dalam km: " + km + " km");
        System.out.println("\n========================================\n");
        m = sisa_jarak / m1;
        sisa_jarak = sisa_jarak % m1;
        System.out.println("Jarak dalam m: " + m + " m");
        System.out.println("\n========================================\n");
        cm = sisa_jarak;
        System.out.println("Jarak dalam cm: " + cm + " cm");
        System.out.println("\n========================================\n");
        System.out.println("Jadi total jarak yg ditempuh adalah: " + km 
                + " km " + m + " m " + cm + " cm ");
    }
}
