package id.fachridantm.pertemuan.ketiga;

public class LogicalOperator {
    public static void main(String[] args) {
        int x = 7, y = 11, z = 11;
        System.out.println("x = " + x);
        System.out.println("y = " + y);
        System.out.println("z = " + z);
        System.out.println("x < " + (x < y));
        System.out.println("x > " + (x > y));
        System.out.println("y <= " + (y <= z));
        System.out.println("x >= " + (x >= y));
        System.out.println("y == z " + (y == z));
        System.out.println("x != y = " + (x != y));
    }
}
