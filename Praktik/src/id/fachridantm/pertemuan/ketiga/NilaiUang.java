package id.fachridantm.pertemuan.ketiga;

import java.util.Scanner;

public class NilaiUang {

    public static void main(String[] args) {
        int nilai_uang, sisa;
        final int p1, p2, p3, p4, p5;
        p1 = 1000;
        p2 = 500;
        p3 = 100;
        p4 = 50;
        p5 = 25;
        int jml_p1, jml_p2, jml_p3, jml_p4, jml_p5;
        Scanner in = new Scanner(System.in);

        System.out.println("Masukan nilai uang: ");
        nilai_uang = in.nextInt();
        jml_p1 = nilai_uang / p1;
        sisa = nilai_uang % p1;
        System.out.println("Jumlah pecahan pertama: " + jml_p1 
                + " lembar uang 1000");
        System.out.println("\n========================================\n");
        jml_p2 = sisa / p2;
        sisa = nilai_uang % p2;
        System.out.println("Jumlah pecahan kedua: " + jml_p2
                + " lembar uang 500");
        System.out.println("\n========================================\n");
        jml_p3 = sisa / p3;
        sisa = nilai_uang % p3;
        System.out.println("Jumlah pecahan ketiga: " + jml_p3
         + " lembar uang 100");
        System.out.println("\n========================================\n");
        jml_p4 = sisa / p4;
        sisa = nilai_uang % p4;
        System.out.println("Jumlah pecahan keempat: " + jml_p4 
                + " lembar uang 50");
        System.out.println("\n========================================\n");
        jml_p5 = sisa / p5;
        System.out.println("Jumlah pecahan kelima: " + jml_p5
                + " lembar uang 25");
        System.out.println("\n========================================\n");
    }
}