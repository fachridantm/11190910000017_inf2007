package id.fachridantm.pertemuan.ketiga;

import java.util.Scanner;

public class SelisihTanggal {
    public static void main(String[] args) {
        int dd1, dd2, dd3, mm1, mm2, mm3, yy1, yy2, yy3;
        int TotalHari1, TotalHari2, SelisihHari, Sisa;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan tanggal pertama: ");
        dd1 = in.nextInt();
        System.out.println("Masukan bulan: ");
        mm1 = in.nextInt();
        System.out.println("Masukan tahun: ");
        yy1 = in.nextInt();
        System.out.println("\nTanggal pertama: " + dd1 + "/" + mm1 + "/" + yy1);
        System.out.println("======================================");
        System.out.println("\nMasukan tanggal kedua: ");
        dd2 = in.nextInt();
        System.out.println("Masukan bulan: ");
        mm2 = in.nextInt();
        System.out.println("Masukan tahun: "); 
        yy2 = in.nextInt();
        System.out.println("\nTanggal Kedua: " + dd2 + "/" + mm2 + "/" + yy2);
        System.out.println("======================================");
        
        TotalHari1 = ((yy1*365) + (mm1*30) + dd1);
        System.out.println("Jumlah hari pada tanggal pertama: " + TotalHari1);
        TotalHari2 = ((yy2*365) + (mm2*30) + dd2);
        System.out.println("Jumlah hari pada tanggal kedua: " + TotalHari2);
        SelisihHari = TotalHari2 - TotalHari1;
        System.out.println("Selisih Hari dari kedua tanggal tersebut adalah: "
                + SelisihHari);
        
        yy3 = SelisihHari / 365;
        Sisa = SelisihHari % 365;
        mm3 = Sisa / 30;
        dd3 = Sisa % 30;
        System.out.println("Jadi, tanggal dari selisih kedua hari "
                + "tersebut adalah: " + dd3 + "/" + mm3 + "/" + yy3);
                        
    }
}
