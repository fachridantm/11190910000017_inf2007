package id.fachridantm.pertemuan.ketujuh;

import java.util.Scanner;

public class AplikasiBulan {
    public static void main(String[] args) {
        int noBulan;
        Scanner in = new Scanner(System.in);
        Bulan bulan = new Bulan();
        
        System.out.println("Masukan nomor bulan: ");
        noBulan = in.nextInt();
        System.out.println(noBulan + " adalah bulan " + bulan.getNama(noBulan));
    }
}
