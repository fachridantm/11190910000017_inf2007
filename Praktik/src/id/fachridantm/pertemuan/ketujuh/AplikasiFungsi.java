package id.fachridantm.pertemuan.ketujuh;

public class AplikasiFungsi {
    public static void main(String[] args) {
        float x;
        Fungsi fungsi = new Fungsi();
        
        System.out.println(" ------------------------------- ");
        System.out.println("        x          |          f(x)        ");
        System.out.println(" ------------------------------- ");
        
        x = (float) 10.0;
        while (x <= 15.0) {
            System.out.println(x + "                     " + fungsi.getHasil(x));
            x = (float) (x + 0.2);
        }
        fungsi.getInfo();
    }
}
