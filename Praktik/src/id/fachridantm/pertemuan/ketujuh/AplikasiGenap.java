package id.fachridantm.pertemuan.ketujuh;

import java.util.Scanner;

public class AplikasiGenap {
    public static void main(String[] args) {
        int bilangan;
        Scanner in = new Scanner(System.in);
        Genap genap = new Genap();
        
        System.out.println("Masukan angka: ");
        bilangan = in.nextInt();
        
        if (genap.getHasil(bilangan)) {
            System.out.println(bilangan + " adalah bilangan genap");
        } else {
            System.out.println(bilangan + " bukan bilangan genap");
        }
    }
}
