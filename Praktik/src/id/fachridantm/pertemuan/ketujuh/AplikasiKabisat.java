package id.fachridantm.pertemuan.ketujuh;

import java.util.Scanner;

public class AplikasiKabisat {
    public static void main(String[] args) {
        int tahun;
        Scanner in = new Scanner(System.in);
        Kabisat kabisat = new Kabisat();
        
        System.out.println("Masukan tahun: ");
        tahun = in.nextInt();
        
        if (kabisat.Kabisat(tahun)) {
            System.out.println(tahun + " adalah tahun kabisat");
        } else {
            System.out.println(tahun + " bukan tahun kabisat");
        }
    }
}
