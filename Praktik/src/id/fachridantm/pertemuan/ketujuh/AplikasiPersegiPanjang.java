package id.fachridantm.pertemuan.ketujuh;

import java.util.Scanner;

public class AplikasiPersegiPanjang {
    public static void main(String[] args) {
        // contoh pemanggilan objek konstruktor default
        PersegiPanjang persegiPanjang2 = new PersegiPanjang();

        Scanner in = new Scanner(System.in);
        double panjang, lebar;

        System.out.println("Masukan Panjang: ");
        panjang = in.nextDouble();

        System.out.println("Masukan lebar: ");
        lebar = in.nextDouble();

        PersegiPanjang persegiPanjang = new PersegiPanjang(panjang, lebar);
        persegiPanjang.getInfo();
        System.out.println("Luas: " + persegiPanjang.getLuas());
        System.out.println("Keliling: " + persegiPanjang.getKeliling());
    }
}
