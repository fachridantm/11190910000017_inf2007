package id.fachridantm.pertemuan.ketujuh;

import java.util.Scanner;

public class AplikasiSegitigaParameter {
    public static void main(String[] args) {
        SegitigaParameter segitiga = new SegitigaParameter();
        
        int i, N;
        double a, t;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Tentukan banyaknya segitiga: ");
        N = in.nextInt();
        for (i = 1; i <= N; i++) {
            System.out.println("Masukan panjang alas: ");
            a = in.nextDouble();
            System.out.println("Masukan tinggi: ");
            t = in.nextDouble();
            SegitigaParameter segitiga2 = new SegitigaParameter(a,t);
        }
    }
}
