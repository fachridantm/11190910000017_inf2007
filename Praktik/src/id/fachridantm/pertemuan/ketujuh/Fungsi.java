package id.fachridantm.pertemuan.ketujuh;

public class Fungsi {
    
    public Fungsi() {
        System.out.println("======= TABEL NILAI ======");
    }
    public float getHasil(float x) {
        return 2*x*x + 5*x - 8;
    }
    public void getInfo() {
        System.out.println(" ------------------------------- ");
    }
}
