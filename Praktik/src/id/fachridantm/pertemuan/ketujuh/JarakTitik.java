package id.fachridantm.pertemuan.ketujuh;

import static java.lang.Math.sqrt;
import java.util.Scanner;

public class JarakTitik {
    
    float X1, X2, Y1, Y2;
    float jarak;
    
    public float JarakTitik() {
        Scanner in = new Scanner(System.in);
        System.out.print("Nilai X1: ");
        X1 = in.nextFloat();
        System.out.print("Nilai X2: ");
        X2 = in.nextFloat();
        System.out.print("Nilai Y1: ");
        Y1 = in.nextFloat();
        System.out.print("Nilai Y2: ");
        Y2 = in.nextFloat();
        return jarak = (float) sqrt((X1 - X2) + (Y1 - Y2) * (Y1 - Y2));
    }
}