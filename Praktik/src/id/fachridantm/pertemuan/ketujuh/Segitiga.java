package id.fachridantm.pertemuan.ketujuh;

import java.util.Scanner;

public class Segitiga {
    private double alas, tinggi, luas;
    Scanner in = new Scanner(System.in);
    
    public Segitiga() {
        
        System.out.println("===Luas Segitiga===");
        
        System.out.println("Masukan panjang alas: ");
        alas = in.nextDouble();
        
        System.out.println("Masukan tinggi: ");
        tinggi = in.nextDouble();
        
        luas = alas * tinggi / 2;
        System.out.println("Luas Segitiga: " + luas);
    }     
}
