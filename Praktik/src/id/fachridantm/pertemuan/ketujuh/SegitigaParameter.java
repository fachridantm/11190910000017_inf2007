package id.fachridantm.pertemuan.ketujuh;

public class SegitigaParameter {
    private int luas;
        
    public SegitigaParameter() {
        System.out.println("===Konstruktor Segitiga===");
    }
    public SegitigaParameter(double alas, double tinggi) {
        this.luas = (int) (alas * tinggi / 2);
        System.out.println("Luas Segitiga: " + luas);
    }
}
