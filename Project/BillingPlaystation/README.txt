# Aplikasi Billing Warnet #

Repository ini berisi Contoh Aplikasi Billing Playstation Sederhana menggunakan Java yang berbentuk CLI (Command Line Interface)



## Konfigurasi ##

* Ubah lokasi `FILE` pada class `BillingController`

* Contoh file `billingplaystation.json` berada pada folder `src`

* Tambahkan library `gson-2.8.6.jar`, file tersedia di folder `lib`



## Cara Menjalankan dan Kompilasi ##

* Pilih menu `Run` -> `Clean and Build Project` atau bisa langsung `Shift + F11`

* Terbuat folder `dist`, masuk ke folder tersebut

* Buka cmd dan direktori file jar tersebut. Ketik perintah `java -jar BillingPlaystation.jar`