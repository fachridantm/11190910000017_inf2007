package id.fachridantm.billingplaystation.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
/**
 *
 * @author Payoy
 */
public class Billing implements Serializable {
    
    private static final long serialVersionUID = -6756463875294313469L;
    
    private String user;
    private int jenisPaket;
    private int paketReg;
    private int paketMal;
    private LocalDateTime waktuLogin;
    private LocalDateTime waktuLogout;
    private BigDecimal biaya;
    private boolean mati = false;
    
    public Billing() {
        
    }
    
    public Billing(String user, int jenisPaket, int paketReg, int paketMal, LocalDateTime waktuLogin, LocalDateTime waktuLogout, BigDecimal biaya) {
        this.user = user;
        this.jenisPaket = jenisPaket;
        this.paketReg = paketReg;
        this.paketMal = paketMal;
        this.waktuLogin = waktuLogin;
        this.waktuLogout = waktuLogout;
        this.biaya = biaya;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getPaketReg() {
        return paketReg;
    }

    public void setPaketReg(int paketReg) {
        this.paketReg = paketReg;
    }

    public int getPaketMal() {
        return paketMal;
    }

    public void setPaketMal(int paketMal) {
        this.paketMal = paketMal;
    }

    public int getJenisPaket() {
        return jenisPaket;
    }

    public void setJenisPaket(int jenisPaket) {
        this.jenisPaket = jenisPaket;
    }

    public LocalDateTime getWaktuLogin() {
        return waktuLogin;
    }

    public void setWaktuLogin(LocalDateTime waktuLogin) {
        this.waktuLogin = waktuLogin;
    }

    public LocalDateTime getWaktuLogout() {
        return waktuLogout;
    }

    public void setWaktuLogout(LocalDateTime waktuLogout) {
        this.waktuLogout = waktuLogout;
    }

    public BigDecimal getBiaya() {
        return biaya;
    }

    public void setBiaya(BigDecimal biaya) {
        this.biaya = biaya;
    }

    public boolean isMati() {
        return mati;
    }

    public void setMati(boolean mati) {
        this.mati = mati;
    }
    
    @Override
    public String toString() {
        return "Billing Warnet {" + "Nama Pengguna = " + user + ", Jenis Paket = " + jenisPaket + ", Waktu Login = " + waktuLogin + ", Waktu Logout = " + waktuLogout + ", Biaya = " + biaya + ", Matikan Paket = " + mati + '}';
    }
}
