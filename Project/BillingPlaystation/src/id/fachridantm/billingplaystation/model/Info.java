package id.fachridantm.billingplaystation.model;

/**
 *
 * @author Payoy
 */
public class Info {
    private final String aplikasi = "Aplikasi Billing Playstation Sederhana";
    private final String version = "Versi 1.0.0";
    
    public String getAplikasi() {
        return aplikasi;
    }
    
    public String getVersion() {
        return version;
    }
}
