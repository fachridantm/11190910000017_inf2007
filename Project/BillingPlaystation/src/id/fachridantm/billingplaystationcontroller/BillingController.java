package id.fachridantm.billingplaystationcontroller;

import com.google.gson.Gson;
import id.fachridantm.billingplaystation.model.Billing;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Payoy
 */
public class BillingController {

    private static final String File = "D:\\Documents\\Payoy\\Tugas Kuliah\\Dasar - Dasar Pemrograman\\GitLab\\11190910000017_inf2007\\Project\\BillingPlaystation\\src\\billingplaystation.json";
    private Billing bw;
    private Scanner in;
    private String user;
    private int jenisPaket;
    private final LocalDateTime waktuLogin;
    private LocalDateTime waktuLogout;
    private final DateTimeFormatter dateTimeFormat;
    private int pilihan;
    private int paketReg;
    private int paketMal;
    private long jam, menit, detik;

    public BillingController() {
        in = new Scanner(System.in);
        waktuLogin = LocalDateTime.now();
        waktuLogout = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    }

    public void setPaketBilling() {

        System.out.println("=================================================");
        System.out.println("MENU PAKET BILLING");
        System.out.println("-------------------------------------------------");

        System.out.println("DAFTAR PAKET");
        System.out.println("1. Paket Reguler");
        System.out.println("2. Paket Malam");
        System.out.println("=================================================");
        System.out.print("Pilih Menu (1/2) : ");

        while (!in.hasNextInt()) {
            String input = in.next();
            System.err.printf("\"%s\" is not a valid number.\n", input);
            System.out.print("Pilih Menu (1/2) : ");
        }
        jenisPaket = in.nextInt();

        setPilihPaket();
    }

    public void setPilihPaket() {
        bw = new Billing();
        in = new Scanner(System.in);

        switch (jenisPaket) {
            case 1:
                setMenuPaketReguler();

                if (paketReg == 1) {
                    bw.setBiaya(new BigDecimal(1000));
                } else if (paketReg == 2) {
                    bw.setBiaya(new BigDecimal(2000));
                } else if (paketReg == 3) {
                    bw.setBiaya(new BigDecimal(3000));
                } else if (paketReg == 4) {
                    bw.setBiaya(new BigDecimal(5000));
                } else if (paketReg == 5) {
                    bw.setBiaya(new BigDecimal(7000));
                } else if (paketReg == 6) {
                    bw.setBiaya(new BigDecimal(8000));
                } else if (paketReg == 7) {
                    bw.setBiaya(new BigDecimal(10000));
                }

                System.out.print("\nMasukan Nama Pengguna: ");
                user = in.next();

                String WaktuLogin = waktuLogin.format(dateTimeFormat);
                System.out.println("Waktu Login : " + WaktuLogin);

                bw.setUser(user.toUpperCase());
                bw.setWaktuLogin(waktuLogin);
                bw.setJenisPaket(jenisPaket);
                bw.setPaketReg(paketReg);
                bw.getBiaya();

                setWritePaket(File, bw);

                System.out.println("");
                System.out.println("Apakah Anda ingin matikan paket?");
                System.out.print("1) Ya, 2) Tidak : ");

                while (!in.hasNextInt()) {
                    String input = in.next();
                    System.err.printf("\"%s\" is not a valid number.\n", input);
                    System.out.println("Apakah Anda ingin matikan paket?");
                    System.out.print("1) Ya, 2) Tidak : ");
                }
                pilihan = in.nextInt();

                if (pilihan == 2) {
                    Menu m = new Menu();
                    m.getMenuAwal();
                } else if (pilihan == 1) {
                    setMatikanPaket();
                }
                break;
            case 2:
                setMenuPaketMalam();

                if (paketMal == 1) {
                    bw.setBiaya(new BigDecimal(8000));
                } else if (paketMal == 2) {
                    bw.setBiaya(new BigDecimal(12000));
                }

                System.out.print("\nMasukan Nama Pengguna: ");
                user = in.next();

                String formatWaktuLogin = waktuLogin.format(dateTimeFormat);
                System.out.println("Waktu Login : " + formatWaktuLogin);

                bw.setUser(user.toUpperCase());
                bw.setWaktuLogin(waktuLogin);
                bw.setJenisPaket(jenisPaket);
                bw.setPaketMal(paketMal);
                bw.getBiaya();

                setWritePaket(File, bw);

                System.out.println("Apakah Anda ingin matikan paket?");
                System.out.print("1) Ya, 2) Tidak : ");

                while (!in.hasNextInt()) {
                    String input = in.next();
                    System.err.printf("\"%s\" is not a valid number.\n", input);
                    System.out.println("Apakah Anda ingin matikan paket?");
                    System.out.print("1) Ya, 2) Tidak : ");
                }
                pilihan = in.nextInt();

                if (pilihan == 2) {
                    Menu m = new Menu();
                    m.getMenuAwal();
                } else if (pilihan == 1) {
                    setMatikanPaket();
                }
                break;
            default:
                setPaketBilling();
                break;
        }
    }

    public void setMenuPaketReguler() {

        System.out.println("=================================================");
        System.out.println("Paket Reguler");
        System.out.println("-------------------------------------------------");
        System.out.println("  KODE  |  NAMA PAKET  |  WAKTU  |   TARIF   ");
        System.out.println("-------------------------------------------------");
        System.out.println("1. Reguler - Seceng | 20 Menit | Rp 1000");
        System.out.println("2. Reguler - Bokek | 40 Menit | Rp 2000");
        System.out.println("3. Reguler - Personal | 60 Menit | Rp 3000");
        System.out.println("4. Reguler - Gamers Normal | 120 Menit| Rp 5000");
        System.out.println("5. Reguler - Gabut | 180 Menit | Rp 7000");
        System.out.println("6. Reguler - Bokek Berkelas | 240 Menit | Rp 8000");
        System.out.println("7. Reguler - Mabar | 300 Menit | Rp 10000");
        System.out.println("8. Keluar Paket");
        System.out.println("=================================================");
        System.out.print("Pilih No. Paket (1-8) : ");

        while (!in.hasNextInt()) {
            String input = in.next();
            System.err.printf("\"%s\" is not a valid number.\n", input);
            System.out.print("Pilih No. Paket (1-8) : ");
        }
        paketReg = in.nextInt();
        setPaketReguler();
    }

    public void setPaketReguler() {

        switch (paketReg) {
            case 1:

                System.out.println("Apakah Anda yakin membeli paket ini?");
                System.out.print("1) Ya,  2) Tidak : ");
                while (!in.hasNextInt()) {
                    String input = in.next();
                    System.err.printf("\"%s\" is not a valid number.\n", input);
                    System.out.println("Apakah Anda yakin membeli paket ini?");
                    System.out.print("1) Ya, 2) Tidak : ");
                }
                pilihan = in.nextInt();

                if (pilihan == 1) {
                    System.out.println("");
                    System.out.println("Selamat Paket " + paketReg + " Berhasil Diaktifkan!");
                } else if (pilihan == 2) {
                    setMenuPaketReguler();
                }
                break;
            case 2:

                System.out.println("Apakah Anda yakin membeli paket ini?");
                System.out.print("1) Ya,  2) Tidak : ");

                while (!in.hasNextInt()) {
                    String input = in.next();
                    System.err.printf("\"%s\" is not a valid number.\n", input);
                    System.out.println("Apakah Anda yakin membeli paket ini?");
                    System.out.print("1) Ya, 2) Tidak : ");
                }
                pilihan = in.nextInt();

                if (pilihan == 1) {
                    System.out.println("");
                    System.out.println("Selamat Paket " + paketReg + " Berhasil Diaktifkan!");
                } else if (pilihan == 2) {
                    setMenuPaketReguler();
                }
                break;
            case 3:

                System.out.println("Apakah Anda yakin membeli paket ini?");
                System.out.print("1) Ya,  2) Tidak : ");

                while (!in.hasNextInt()) {
                    String input = in.next();
                    System.err.printf("\"%s\" is not a valid number.\n", input);
                    System.out.println("Apakah Anda yakin membeli paket ini?");
                    System.out.print("1) Ya, 2) Tidak : ");
                }
                pilihan = in.nextInt();

                if (pilihan == 1) {
                    System.out.println("");
                    System.out.println("Selamat Paket " + paketReg + " Berhasil Diaktifkan!");
                } else if (pilihan == 2) {
                    setMenuPaketReguler();
                }
                break;
            case 4:

                System.out.println("Apakah Anda yakin membeli paket ini?");
                System.out.print("1) Ya,  2) Tidak : ");

                while (!in.hasNextInt()) {
                    String input = in.next();
                    System.err.printf("\"%s\" is not a valid number.\n", input);
                    System.out.println("Apakah Anda yakin membeli paket ini?");;
                    System.out.print("1) Ya, 2) Tidak : ");
                }
                pilihan = in.nextInt();

                if (pilihan == 1) {
                    System.out.println("");
                    System.out.println("Selamat Paket " + paketReg + " Berhasil Diaktifkan!");
                } else if (pilihan == 2) {
                    setMenuPaketReguler();
                }
                break;
            case 5:

                System.out.println("Apakah Anda yakin membeli paket ini?");
                System.out.print("1) Ya,  2) Tidak : ");
                while (!in.hasNextInt()) {
                    String input = in.next();
                    System.err.printf("\"%s\" is not a valid number.\n", input);
                    System.out.println("Apakah Anda yakin membeli paket ini?");
                    System.out.print("1) Ya, 2) Tidak : ");
                }
                pilihan = in.nextInt();

                if (pilihan == 1) {
                    System.out.println("");
                    System.out.println("Selamat Paket " + paketReg + " Berhasil Diaktifkan!");
                } else if (pilihan == 2) {
                    setMenuPaketReguler();
                }
                break;
            case 6:

                System.out.println("Apakah Anda yakin membeli paket ini?");
                System.out.print("1) Ya,  2) Tidak : ");
                while (!in.hasNextInt()) {
                    String input = in.next();
                    System.err.printf("\"%s\" is not a valid number.\n", input);
                    System.out.println("Apakah Anda yakin membeli paket ini?");
                    System.out.print("1) Ya, 2) Tidak : ");
                }
                pilihan = in.nextInt();

                if (pilihan == 1) {
                    System.out.println("");
                    System.out.println("Selamat Paket " + paketReg + " Berhasil Diaktifkan!");
                } else if (pilihan == 2) {
                    setMenuPaketReguler();
                }
                break;
            case 7:

                System.out.println("Apakah Anda yakin membeli paket ini?");
                System.out.print("1) Ya,  2) Tidak : ");
                while (!in.hasNextInt()) {
                    String input = in.next();
                    System.err.printf("\"%s\" is not a valid number.\n", input);
                    System.out.println("Apakah Anda yakin membeli paket ini?");
                    System.out.print("1) Ya, 2) Tidak : ");
                }
                pilihan = in.nextInt();

                if (pilihan == 1) {
                    System.out.println("");
                    System.out.println("Selamat Paket " + paketReg + " Berhasil Diaktifkan!");
                } else if (pilihan == 2) {
                    setMenuPaketReguler();
                }
                break;
            case 8:
                setPaketBilling();
                break;
            default:
                setMenuPaketReguler();
                break;
        }
    }

    public void setMenuPaketMalam() {

        System.out.println("=================================================");
        System.out.println("Paket Malam");
        System.out.println("-------------------------------------------------");
        System.out.println("  No  |  NAMA PAKET  |  WAKTU  |   TARIF   ");
        System.out.println("-------------------------------------------------");
        System.out.println("1. Malam - Santuy | 21:00 - 00:00 | Rp 8000");
        System.out.println("2. Malam - Berkah | 23:00 - 05:00 | Rp 12000");
        System.out.println("3. Keluar Paket");
        System.out.println("=================================================");
        System.out.print("Pilih No. Paket (1/2/3) : ");

        while (!in.hasNextInt()) {
            String input = in.next();
            System.err.printf("\"%s\" is not a valid number.\n", input);
            System.out.print("Pilih No. Paket (1/2/3) : ");
        }
        paketMal = in.nextInt();
        setPaketMalam();
    }

    public void setPaketMalam() {

        switch (paketMal) {
            case 1:

                System.out.println("Apakah Anda yakin membeli paket ini?");
                System.out.print("1) Ya,  2) Tidak : ");
                while (!in.hasNextInt()) {
                    String input = in.next();
                    System.err.printf("\"%s\" is not a valid number.\n", input);
                    System.out.println("Apakah Anda yakin membeli paket ini?");
                    System.out.print("1) Ya, 2) Tidak : ");
                }
                pilihan = in.nextInt();

                if (pilihan == 1) {
                    System.out.println("");
                    System.out.println("Selamat Paket " + paketMal + " Berhasil Diaktifkan!");
                } else if (pilihan == 2) {
                    setMenuPaketMalam();
                }
                break;
            case 2:

                System.out.println("Apakah Anda yakin membeli paket ini?");
                System.out.print("1) Ya,  2) Tidak : ");

                while (!in.hasNextInt()) {
                    String input = in.next();
                    System.err.printf("\"%s\" is not a valid number.\n", input);
                    System.out.println("Apakah Anda yakin membeli paket ini?");
                    System.out.print("1) Ya, 2) Tidak : ");
                }
                pilihan = in.nextInt();

                if (pilihan == 1) {
                    System.out.println("");
                    System.out.println("Selamat Paket " + paketMal + " Berhasil Diaktifkan!");
                } else if (pilihan == 2) {
                    setMenuPaketMalam();
                }
                break;
            case 3:
                setPaketBilling();
                break;
            default:
                setMenuPaketMalam();
                break;
        }
    }

    public void setMatikanPaket() {

        System.out.print("\nMasukan Nama Pengguna: ");
        user = in.next();

        Billing b = getSearch(user);
        if (b != null) {
            LocalDateTime tempWaktu = LocalDateTime.from(b.getWaktuLogin());
            waktuLogout = LocalDateTime.now();
            jam = tempWaktu.until(waktuLogout, ChronoUnit.HOURS);
            menit = ((tempWaktu.until(waktuLogout, ChronoUnit.MINUTES)) % 60);
            detik = ((tempWaktu.until(waktuLogout, ChronoUnit.SECONDS)) % 60);
            b.setWaktuLogout(waktuLogout);
            b.setMati(true);

            System.out.println("");
            System.out.println("Nama Pengguna : " + b.getUser());
            System.out.println("Jenis Paket : " + (b.getJenisPaket() == 1 ? "Reguler" : "Malam"));
            System.out.println("Waktu Login : " + b.getWaktuLogin().format(dateTimeFormat));
            System.out.println("Waktu Logout : " + b.getWaktuLogout().format(dateTimeFormat));
            System.out.println("Lama : " + jam + " Jam " + menit + " Menit " + detik + " Detik");
            System.out.println("Biaya : " + b.getBiaya());
            System.out.println("");

            System.out.println("Apakah Anda mau melakukan proses bayar pada Pengguna ini?");
            System.out.print("1) Ya, 2) Tidak, 3) Keluar : ");
            while (!in.hasNextInt()) {
                String input = in.next();
                System.err.printf("\"%s\" is not a valid number.\n", input);
                System.out.println("Apakah Anda mau melakukan proses bayar pada Pengguna ini?");
                System.out.print("1) Ya, 2) Tidak, 3) Keluar : ");
            }
            pilihan = in.nextInt();

            switch (pilihan) {
                case 1:
                    setWritePaket(File, b);
                    break;
                case 2:
                    setMatikanPaket();
                    break;
                default:
                    Menu m = new Menu();
                    m.getMenuAwal();
                    break;
            }

            System.out.println("Apakah mau membeli paket lagi?");
            System.out.print("1) Ya, 2) Tidak : ");
            while (!in.hasNextInt()) {
                String input = in.next();
                System.err.printf("\"%s\" is not a valid number.\n", input);
                System.out.println("Apakah Anda mau membeli paket lagi?");
                System.out.print("1) Ya, 2) Tidak : ");
            }
            pilihan = in.nextInt();

            if (pilihan == 2) {
                Menu m = new Menu();
                m.getMenuAwal();
            } else if (pilihan == 1) {
                setPilihPaket();
            }
        } else {
            System.err.println("Data tidak ditemukan, tolong input user dengan benar");
            setMatikanPaket();
        }
    }

    public void setWritePaket(String File, Billing b) {
        Gson gson = new Gson();

        List<Billing> paket = getReadBilling(File);
        paket.remove(b);
        paket.add(b);

        String json = gson.toJson(paket);
        try {
            FileWriter writer = new FileWriter(File);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(BillingController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Billing> getReadBilling(String File) {
        List<Billing> paket = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try (Reader reader = new FileReader(File)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                Billing[] bil = gson.fromJson(line, Billing[].class);
                paket.addAll(Arrays.asList(bil));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BillingController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BillingController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return paket;
    }

    public Billing getSearch(String user) {

        List<Billing> paket = getReadBilling(File);

        Billing b = paket.stream()
                .filter(pp -> user.equalsIgnoreCase(pp.getUser()))
                .findAny()
                .orElse(null);
        return b;
    }

    public void getDataBilling() {

        List<Billing> paket = getReadBilling(File);
        Predicate<Billing> isMati = e -> e.isMati() == true;
        Predicate<Billing> isDate = e -> e.getWaktuLogout().toLocalDate().equals(LocalDate.now());

        List<Billing> bResults = paket.stream().filter(isMati.and(isDate)).collect(Collectors.toList());
        BigDecimal total = bResults.stream()
                .map(Billing::getBiaya)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("Nama Pengguna \tWaktu Login \t\tWaktu Logout \t\tBiaya");
        System.out.println("--------- \t----------- \t\t------------ \t\t-----");
        bResults.forEach((b) -> {
            System.out.println(b.getUser() + "\t\t" + b.getWaktuLogin().format(dateTimeFormat) + "\t" + b.getWaktuLogout().format(dateTimeFormat) + "\t" + b.getBiaya());
        });
        System.out.println("--------- \t----------- \t\t------------ \t\t-----");
        System.out.println("====================================");
        System.out.println("Pendapatan Total = Rp. " + total);
        System.out.println("====================================");

        System.out.println("Apakah Anda mau membeli paket lagi?");
        System.out.print("1) Ya, 2) Tidak : ");
        while (!in.hasNextInt()) {
            String input = in.next();
            System.err.printf("\"%s\" is not a valid number.\n", input);
            System.out.println("Apakah Anda mau membeli paket lagi?");
            System.out.print("1) Ya, 2) Tidak : ");
        }
        pilihan = in.nextInt();

        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            pilihan = 1;
            setPaketBilling();
        }
    }
}
