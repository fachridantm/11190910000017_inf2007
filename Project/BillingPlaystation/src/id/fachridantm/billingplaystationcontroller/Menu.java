package id.fachridantm.billingplaystationcontroller;

import id.fachridantm.billingplaystation.model.Info;
import java.util.Scanner;
/**
 *
 * @author Payoy
 */
public class Menu {
    
    private final Scanner in = new Scanner(System.in);
    private int noMenu;
    BillingController bc;

    public void getMenuAwal() { 
        Info info = new Info();
        
        System.out.println("=================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getVersion());
        System.out.println("-------------------------------------------------");
        
        System.out.println("DAFTAR MENU");
        System.out.println("1. Menu Paket Billing");
        System.out.println("2. Matikan Paket");
        System.out.println("3. Laporan Harian");
        System.out.println("4. Keluar Aplikasi");
        System.out.println("=================================================");
        System.out.print("Pilih Menu (1/2/3/4) : ");
        
        
            while (!in.hasNextInt()) {
                String input = in.next();
                System.err.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Pilih Menu (1/2/3/4) : ");
            }
            noMenu = in.nextInt();
        
        setPilihMenu();
    }
    
    public void setPilihMenu() {
        bc = new BillingController();
        switch (noMenu) {
            case 1:
                bc.setPaketBilling();
                break;
            case 2:
                bc.setMatikanPaket();
                break;
            case 3:
                bc.getDataBilling();
                break;
            case 4:
                System.out.println("Sampai jumpa :)");
                System.exit(0);
                break;
            default:
                getMenuAwal();
        }
    }    
}
